export const environment = {
  production: false,
  auth: {
    clientId: 'OJE6cEj98FEP4CsXu_D8YeDgZ_OX9O5_GujAfpY2j2WNq8yauo6jHczE-5sh_R_1',
    redirectUri: 'http://localhost:4200/callback',
    domain: 'https://api.genius.com/oauth/authorize'
  },
  genius: {
    apiUrl: 'https://api.genius.com',
  }
};
