export const environment = {
  production: false,
  auth: {
    clientId: 'i4rBWuFNy53eHhvMU1wKBkMaunQ21GHP_DdUUR6TgPjXKEMYTsYQxC1esQGjh-_f',
    redirectUri: 'http://localhost:4200/callback',
    domain: 'https://api.genius.com/oauth/authorize'
  },
  genius: {
    apiUrl: 'https://api.genius.com'
  }
};
