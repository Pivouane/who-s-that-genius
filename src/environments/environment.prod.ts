export const environment = {
  production: true,
  auth: {
    clientId: 'i4rBWuFNy53eHhvMU1wKBkMaunQ21GHP_DdUUR6TgPjXKEMYTsYQxC1esQGjh-_f',
    redirectUri: 'https://who-s-that-genius-pivouane-1e522707cd3e76993df67ef5bcfe55ef2592.gitlab.io/callback',
    domain: 'https://api.genius.com/oauth/authorize',
  },
  genius: {
    apiUrl: 'https://api.genius.com',
  }
};
