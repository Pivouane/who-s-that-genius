import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class GameService {

  constructor() { }

  saveGameResult(result: string, score: number) {
    const gameData = {
      result,
      score
    };

    const gameHistory = this.getGameHistory();
    gameHistory.push(gameData);
    localStorage.setItem('gameHistory', btoa(JSON.stringify(gameHistory)));
  }

  getGameHistory() {
    const gameHistory = localStorage.getItem('gameHistory');
    if (gameHistory) {
      return JSON.parse(atob(gameHistory));
    }
    return [];
  }
}
