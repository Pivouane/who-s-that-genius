import { Component } from '@angular/core';
import { GameService } from '../game.service';
import { TranslateModule } from '@ngx-translate/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-history',
  standalone: true,
  imports: [
    TranslateModule,
    CommonModule
  ],
  templateUrl: './history.component.html',
  styleUrl: './history.component.less'
})
export class HistoryComponent {
  gameHistory: any[] = [];

  constructor(private gameService: GameService) { }

  ngOnInit(): void {
    this.gameHistory = this.gameService.getGameHistory();
  }
}
