import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-final-score',
  standalone: true,
  imports: [],
  templateUrl: './final-score.component.html',
  styleUrl: './final-score.component.less'
})
export class FinalScoreComponent {
  @Input() score: number = 0;

  refreshPage() {
    window.location.reload();
  }

}
