import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QueryPlayComponent } from './query-play.component';

describe('QueryPlayComponent', () => {
  let component: QueryPlayComponent;
  let fixture: ComponentFixture<QueryPlayComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [QueryPlayComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(QueryPlayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
