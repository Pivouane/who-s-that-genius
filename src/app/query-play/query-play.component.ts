import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { ApiService } from '../api.service';
import { LoadingCircleComponent } from '../loading-circle/loading-circle.component';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-query-play',
  standalone: true,
  imports: [
    TranslateModule,
    LoadingCircleComponent,
    CommonModule
  ],
  templateUrl: './query-play.component.html',
  styleUrl: './query-play.component.less'
})
export class QueryPlayComponent {
  q: string | undefined;
  id: string | undefined;
  artistId: string | undefined;
  song: string | undefined;
  artist: string | undefined;
  loadPage: boolean = false;

  constructor(public route: ActivatedRoute,private translate: TranslateService, private api: ApiService, private router: Router) { }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.q = params['q'];
      
      if (this.q) {
        this.q = this.q.replace(/-/g, ' ');
        this.translate.get('query_play.title', { query: this.q }).subscribe((res: string) => {
        this.q = res;
        });
      }
      this.id = params['id'];
      if (this.id) {
        this.api.getSong(this.id).subscribe((data: any) => {
          this.translate.get('query_play.by_songs.title', { song: data.title }).subscribe((res: string) => {
            this.song = res;
          });
          this.translate.get('query_play.by_artists.title', { artist: data.primary_artist.name }).subscribe((res: string) => {
            this.artist = res;
          });
          this.artistId = data.primary_artist.id;
          this.loadPage = false;
        });
      }
    });
  }

  public playBySong() {
    this.router.navigate(['/s', this.id]);
  }

  public playByArtist() {
    this.router.navigate(['/a', this.artistId]);
  }
}
