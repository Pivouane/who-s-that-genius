import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FeaturedArtistComponent } from './featured-artist.component';

@NgModule({
  declarations: [FeaturedArtistComponent],
  imports: [CommonModule],
  exports: [FeaturedArtistComponent]
})
export class FeaturedArtistModule { }
