import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-featured-artist',
  templateUrl: './featured-artist.component.html',
  styleUrls: ['./featured-artist.component.less']
})
export class FeaturedArtistComponent {
  @Input() artistName: string | undefined;
  @Input() artistLink: string | undefined;
  @Input() artistDescription: string | undefined;
  @Input() artistImageUrl: string | undefined;
}
