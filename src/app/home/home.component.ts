import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { ApiService } from '../api.service';
import { HttpClientModule } from '@angular/common/http';
import { UserInterface } from '../models/api-response.model';
import { CommonModule } from '@angular/common';
import { FeaturedArtistModule } from './featured-artist/featured-artist.module';
import { TranslateModule } from '@ngx-translate/core';

@Component({
  selector: 'app-home',
  standalone: true,
  imports: [
    HttpClientModule,
    CommonModule,
    FeaturedArtistModule,
    TranslateModule
  ],
  templateUrl: './home.component.html',
  styleUrl: './home.component.less',
})
export class HomeComponent implements OnInit {
  user: UserInterface | undefined;
  constructor(private auth: AuthService, private api: ApiService) {}


  
  ngOnInit() {
    this.api.getAccount()
    .subscribe(user => {
      this.user = user
    })
    
  }
}
