import { Component, Input } from '@angular/core';
import { SearchResultInterface } from '../../models/api-response.model';
import { CommonModule } from '@angular/common';
import { Router } from '@angular/router';
import { LoadingCircleComponent } from '../../loading-circle/loading-circle.component';

@Component({
  selector: 'app-search-results',
  standalone: true,
  imports: [
    CommonModule,
    LoadingCircleComponent
  ],
  templateUrl: './search-results.component.html',
  styleUrl: './search-results.component.less'
})
export class SearchResultsComponent {

  constructor(private router: Router) {}

  @Input() searchResults: SearchResultInterface[] = [];

  @Input() loading: boolean = false;

  onClick(searchResult: SearchResultInterface) {
    const queryParams = { q: searchResult.full_title, id: searchResult.id };
    this.router.navigate(['/qplay'], { queryParams });
  
  }
}
