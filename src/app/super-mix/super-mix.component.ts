import { Component } from '@angular/core';
import { ArtistInterface, ReferentInterface, SearchResultInterface } from '../models/api-response.model';
import { FormControl, ReactiveFormsModule } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '../api.service';
import { GameService } from '../game.service';
import { debounceTime, distinctUntilChanged } from 'rxjs';
import { FinalScoreComponent } from '../final-score/final-score.component';
import { CommonModule } from '@angular/common';
import { LoadingCircleComponent } from '../loading-circle/loading-circle.component';
import { TranslateModule } from '@ngx-translate/core';

@Component({
  selector: 'app-super-mix',
  standalone: true,
  imports: [
    FinalScoreComponent,
    CommonModule,
    LoadingCircleComponent,
    ReactiveFormsModule,
    TranslateModule
  ],
  templateUrl: './super-mix.component.html',
  styleUrl: './super-mix.component.less'
})
export class SuperMixComponent {

  loadingPage: boolean = true;
  loadingGame: boolean = true;
  endGame: boolean = false;
  showNextModal: boolean = false;

  index: number = 0;
  score: number = 5000;

  artists: string[] = [];
  currentArtistId: string | undefined;

  currentSong: SearchResultInterface | undefined;

  currentSongReferents: ReferentInterface[] = [];
  shownReferents: ReferentInterface[] = [];

  previousGuesses: SearchResultInterface[] = [];

  guessSuggestionsShown: boolean = false;
  guessSuggestions: SearchResultInterface[] = [];
  loadingGuessSuggestions: boolean = false;

  guess: FormControl = new FormControl('');

  loadingLyrics: boolean = true;

  constructor(private api: ApiService, private game: GameService) { }



  showGuessSuggestions() {
    this.guessSuggestionsShown = true;
  }

  hideGuessSuggestions() {
    this.guessSuggestionsShown = false;
  }

  checkGuess(guessedSong: SearchResultInterface) {
    if (this.previousGuesses.find(song => song.id === guessedSong.id)) {
      return;
    }

    this.previousGuesses.push(guessedSong);
    if (guessedSong.id === this.currentSong?.id) {
      this.showNextModal = true;
    } else {
      this.score -= 100;
    }
  }


  onClickGuess(guessedSong: SearchResultInterface) {
    this.checkGuess(guessedSong);
    this.guessSuggestionsShown = false;
  }


  onSubmitGuess() {
    this.guessSuggestionsShown = false;
    this.api.search(this.guess.value).subscribe((results: SearchResultInterface[]) => {
      const guessedSong = results[0];
      this.checkGuess(guessedSong);
    });
  }

  nextRound() {
    this.index++;
    if (this.index === 5) {
      this.game.saveGameResult('SuperMix', this.score);
      this.endGame = true;
    } else {
      this.setUpGame();
    }
  }

  skipRound() {
    this.score -= 500;
    this.previousGuesses.push(this.currentSong!);
    this.showNextModal = true;
  }


  moreReferents() {
    if (this.currentSongReferents.length === 0) {
      return;
    }
    this.loadingLyrics = true;
    this.score -= 200;
    this.shownReferents.push(this.currentSongReferents[Math.floor(Math.random() * this.currentSongReferents.length)]);
    this.currentSongReferents = this.currentSongReferents.filter(referent => referent.id !== this.shownReferents[this.shownReferents.length - 1].id);
    this.loadingLyrics = false;
  }

  searchGuess(query: string) {
    this.guessSuggestions = [];
    this.showGuessSuggestions();
    this.loadingGuessSuggestions = true;
    this.api.search(query).subscribe((results: SearchResultInterface[]) => {
      this.guessSuggestions = results;
      this.loadingGuessSuggestions = false;
    });
  }

  setUpGame() {
    this.showNextModal = false;
    this.shownReferents = [];
    this.currentSongReferents = [];
    this.previousGuesses = [];
    this.currentArtistId = this.artists[Math.floor(Math.random() * this.artists.length)];

    this.loadingLyrics = true;
    this.api.getArtistSongs(this.currentArtistId!)
      .subscribe((songs: SearchResultInterface[]) => {
        songs = songs.filter(song => song.annotation_count > 5);
        this.currentSong = songs[Math.floor(Math.random() * songs.length)];
        this.api.getReferents(this.currentSong!.id.toString())
          .subscribe((referents: ReferentInterface[]) => {
            this.currentSongReferents = referents;
            this.shownReferents.push(this.currentSongReferents[Math.floor(Math.random() * this.currentSongReferents.length)]);
            this.currentSongReferents = this.currentSongReferents.filter(referent => referent.id !== this.shownReferents[0].id);
            this.loadingLyrics = false;
            this.loadingGame = false;
          });
      });
  }

  ngOnInit() {
    this.api.getArtists().subscribe((artistIds: string[]) => {
      this.artists = artistIds;
      this.setUpGame();
      this.loadingPage = false;
    });

    this.guess.valueChanges.pipe(
      debounceTime(400),
      distinctUntilChanged()
    ).subscribe(query => {
      this.searchGuess(query);
    });
  }

}
