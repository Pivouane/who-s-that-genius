import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SuperMixComponent } from './super-mix.component';

describe('SuperMixComponent', () => {
  let component: SuperMixComponent;
  let fixture: ComponentFixture<SuperMixComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [SuperMixComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(SuperMixComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
