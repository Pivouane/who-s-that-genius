import { Routes } from '@angular/router';
import { CallbackComponent } from './callback/callback.component';
import { HomeComponent } from './home/home.component';
import { authGuard } from './auth.guard';
import { TutorialComponent } from './tutorial/tutorial.component';
import { QueryPlayComponent } from './query-play/query-play.component';
import { SongPlayComponent } from './song-play/song-play.component';
import { ArtistPlayComponent } from './artist-play/artist-play.component';
import { HistoryComponent } from './history/history.component';
import { SuperMixComponent } from './super-mix/super-mix.component';

export const routes: Routes = [
  { path: '', component: HomeComponent , canActivate: [authGuard]},
  { path: 'howtoplay', component: TutorialComponent , canActivate: [authGuard]},
  { path: 'qplay', component: QueryPlayComponent , canActivate: [authGuard]},
  { path: 'a/:id', component: ArtistPlayComponent , canActivate: [authGuard]},
  { path: 's/:id', component: SongPlayComponent , canActivate: [authGuard]},
  { path: 'super-mix', component: SuperMixComponent , canActivate: [authGuard]},
  { path: 'history', component: HistoryComponent , canActivate: [authGuard]},
  { path: 'callback', component: CallbackComponent },
];
