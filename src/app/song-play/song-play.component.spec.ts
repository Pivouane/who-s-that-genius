import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SongPlayComponent } from './song-play.component';

describe('SongPlayComponent', () => {
  let component: SongPlayComponent;
  let fixture: ComponentFixture<SongPlayComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [SongPlayComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(SongPlayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
