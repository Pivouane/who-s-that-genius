import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '../api.service';
import { CommonModule } from '@angular/common';
import { AnnotationInterface, ReferentInterface } from '../models/api-response.model';
import { scan, take, tap } from 'rxjs';
import { LoadingCircleComponent } from '../loading-circle/loading-circle.component';
import { FinalScoreComponent } from "../final-score/final-score.component";
import { TranslateModule } from '@ngx-translate/core';
import { GameService } from '../game.service';

@Component({
    selector: 'app-song-play',
    standalone: true,
    templateUrl: './song-play.component.html',
    styleUrl: './song-play.component.less',
    imports: [
        CommonModule,
        LoadingCircleComponent,
        FinalScoreComponent,
        TranslateModule
    ]
})
export class SongPlayComponent {
  songTitle: string | undefined;
  loadingPage: boolean = true;
  loadingGame: boolean = true;
  endGame: boolean = false;
  showNextModal: boolean = false;
  index: number = 0;
  score: number = 5000;
  correct: ReferentInterface | undefined;
  correctIndex: number = 0;
  solutions: AnnotationInterface[] = [];
  referents: ReferentInterface[] = [];

  constructor(private route: ActivatedRoute, private api: ApiService, private game: GameService) { }

  setUpGame() {
    const randomReferent = this.referents[Math.floor(Math.random() * this.referents.length)];
    this.referents = this.referents.filter((referent) => referent !== randomReferent);

    this.correct = randomReferent;
    const randomSolutions = this.referents.map((referent) => referent.annotations[0]).sort(() => Math.random() - 0.5).slice(0, 2);
    randomSolutions.push(randomReferent.annotations[0]);
    this.solutions = randomSolutions.sort(() => Math.random() - 0.5);
    this.correctIndex = this.solutions.indexOf(randomReferent.annotations[0]);
    this.loadingGame = false;
  }

  ngOnInit() {
    const songId = this.route.snapshot.paramMap.get('id')!;
    this.api.getReferents(songId)
    .subscribe((referents) => {
      this.referents = referents;
      this.setUpGame();
    });
    this.api.getSong(songId).subscribe((song) => {
      this.songTitle = song.full_title;
      this.loadingPage = false;
    });
  }

  nextRound() {
    document.querySelectorAll('.annotation').forEach((element) => {
      element.classList.remove('correct');
      element.classList.remove('incorrect');
    });
    this.loadingGame = true;
    if (this.index === 4) {
      this.game.saveGameResult('win', this.score);
      this.endGame = true;
    } else {
      this.setUpGame();
      this.index++;
    }
    this.showNextModal = false;
  }
  
  onClick(guess: AnnotationInterface, i: number) {
    if (this.showNextModal) {
      return;
    }
    if (guess === this.correct!.annotations[0]) {
      document.getElementsByClassName('annotation')[i].classList.add('correct');
    } else {
      this.score -= 1000;
      document.getElementsByClassName('annotation')[i].classList.add('incorrect');
      document.getElementsByClassName('annotation')[this.correctIndex].classList.add('correct');
    }

    this.showNextModal = true;
  }
}
