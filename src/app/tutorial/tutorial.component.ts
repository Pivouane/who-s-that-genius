import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';

@Component({
  selector: 'app-tutorial',
  standalone: true,
  imports: [
    TranslateModule,
    CommonModule
  ],
  templateUrl: './tutorial.component.html',
  styleUrl: './tutorial.component.less'
})
export class TutorialComponent {
  gameByArtist() {
    throw new Error('Method not implemented.');
  }
  gameBySong() {
    throw new Error('Method not implemented.');
  }

}
