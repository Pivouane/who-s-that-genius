import { Injectable } from '@angular/core';
import { environment } from '../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private readonly clientId = environment.auth.clientId
  private readonly redirectUri = environment.auth.redirectUri
  private readonly scope = 'me create_annotation'
  private readonly responseType = 'token'

  constructor() { }

  private generateState() {
    return Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15)
  }

  private buildAuthUrl() {
    return `${environment.auth.domain}?client_id=${this.clientId}&redirect_uri=${this.redirectUri}&response_type=${this.responseType}&scope=${this.scope}`
  }

  startImplicitFlow() {
    const state = this.generateState()
    localStorage.setItem('state', state)
    
    const url = `${this.buildAuthUrl()}&state=${state}`

    window.location.href = url
  }
  
  handleRedirect() {
    const state = localStorage.getItem('state')
    const hash = window.location.hash
    const params = new URLSearchParams(hash.substring(1))

    if (params.has('state') && params.get('state') === state) {
      localStorage.removeItem('state')
      window.location.href = '/'
    } else {
      console.error('State is invalid')
    }

  }
}
