import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-callback',
  standalone: true,
  imports: [],
  templateUrl: './callback.component.html',
  styleUrl: './callback.component.less'
})
export class CallbackComponent implements OnInit {

  constructor(private authService: AuthService) { }

  ngOnInit(): void {
    const hash = window.location.hash
    const params = new URLSearchParams(hash.substring(1))

    if (params.has('access_token')) {
      const token: string = params.get('access_token')!
      localStorage.setItem('token', token)
      this.authService.handleRedirect()
    } else {
      console.error('No token found')
    }
  }


}
