import { CanActivateFn } from '@angular/router';
import { AuthService } from './auth.service';


export const authGuard: CanActivateFn = (route, state) => {
  const authService = new AuthService();
  const token = localStorage.getItem('token');

  if (!token) {
    authService.startImplicitFlow();
    return false;
  }

  return true;
};
