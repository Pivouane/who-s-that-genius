import { Injectable, NgModule } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../environments/environment';
import { AuthService } from './auth.service';
import { Observable, catchError, concat, map, throwError } from 'rxjs';
import { ApiResponseInterface, ReferentInterface, SearchResultInterface, UserInterface } from './models/api-response.model';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  getArtists(): Observable<string[]> {
    return this.http.get('/assets/genius/featured/artists.json') as Observable<string[]>
  }

  constructor(private auth: AuthService, private http: HttpClient) { }

  authedApiRequest(
    method: string,
    endpoint: string,
    body?: any
  ) {
    let url: string = environment.genius.apiUrl + endpoint

    // add access token as a url parameter
    const token: string = localStorage.getItem('token')!

    if (token) {
      const separator = url.includes('?') ? '&' : '?'
      url += `${separator}access_token=${token}`
    }

    return this.http.request(method, url, { body }).pipe(
      catchError(err => {
        if (err.status === 401) {
          this.auth.startImplicitFlow()
        }
        return throwError(err)
      })
    )
  }

  public getAccount(): Observable<UserInterface> {
    return this.authedApiRequest('GET', '/account')
    .pipe(map((response: Object) => {
      const userResponse = response as ApiResponseInterface
      return userResponse.response.user
    }))
  }

  public search(queryString: string | null): Observable<SearchResultInterface[]> {
    return this.authedApiRequest('GET', '/search?q=' + queryString)
    .pipe(map((response: Object) => {
      const searchResponse = response as ApiResponseInterface
      return searchResponse.response.hits.map((hit) => hit.result)
    }))
  }

  getArtist(id: string) {
    return this.authedApiRequest('GET', '/artists/' + id)
    .pipe(map((response: Object) => {
      const artistResponse = response as ApiResponseInterface
      return artistResponse.response.artist
    }))
  }

  getArtistSongs(id: string) {
    return this.authedApiRequest('GET', '/artists/' + id + '/songs?per_page=50&sort=popularity')
    .pipe(map((response: Object) => {
      return (response as ApiResponseInterface).response.songs
    }))
  }

  
  getSong(id: string) {
    return this.authedApiRequest('GET', '/songs/' + id)
    .pipe(map((response: Object) => {
      const songResponse = response as ApiResponseInterface
      return songResponse.response.song
    }))
  }

  getReferents(id: string, page: number = 1, perPage: number = 50): Observable<ReferentInterface[]>{
    return this.authedApiRequest('GET', `/referents?song_id=${id}&page=${page}&per_page=${perPage}&text_format=plain`)
    .pipe(map((response: Object) => {
      const referentsResponse = response as ApiResponseInterface
      return referentsResponse.response.referents
    }))
  }

}
