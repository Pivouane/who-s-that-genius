import { Component } from '@angular/core';
import { FormControl } from '@angular/forms';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { ApiService } from './api.service';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { UserInterface, SearchResultInterface } from './models/api-response.model';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent {
  title = 'who-s-that-genius';
  user: UserInterface | undefined;
  searchResults: SearchResultInterface[] = [];
  query = new FormControl('');
  searchResultsShown = false;
  loadingSearchResults: boolean = false;

  constructor(
    private api: ApiService,
    private router: Router,
    translate: TranslateService,
    private http: HttpClient
  ) {

    translate.setDefaultLang('en');

    const browserLang = translate.getBrowserLang()!;
    translate.use(browserLang.match(/en|fr/) ? browserLang : 'en');
  }

  hideSearchResults() {
    this.searchResultsShown = false;
  }

  showSearchResults() {
    this.searchResultsShown = true;
  }

  ngOnInit() {
    this.api.getAccount().subscribe((user: UserInterface) => {
      this.user = user;
    });

    this.query.valueChanges.pipe(
      debounceTime(400),
      distinctUntilChanged()
    ).subscribe(query => {
      this.search(query);
    });
  }

  onSubmit() {
    this.searchResultsShown = false;
    this.api.search(this.query.value).subscribe((results: SearchResultInterface[] ) => {
      const queryParams = { q: this.query.value, id: results[0].id };
      this.router.navigate(['/qplay'], { queryParams });
    });
  }

  search(queryString: string | null) {
    this.loadingSearchResults = true;
    this.searchResultsShown = true;
    this.api.search(queryString).subscribe((results: SearchResultInterface[]) => {
      this.searchResults = results;
      this.loadingSearchResults = false;
    });
  }

  
  randomSongGame() {
    this.http.get('/assets/genius/featured/songs.json')
    .subscribe((data: any) => {
      const randomSong = data[Math.floor(Math.random() * data.length)];
      this.router.navigate(['/s', randomSong]);
    });
  }
  randomArtistGame() {
    this.http.get('/assets/genius/featured/artists.json')
    .subscribe((data: any) => {
      const randomArtist = data[Math.floor(Math.random() * data.length)];
      this.router.navigate(['/a', randomArtist]);
    });
  }
}
