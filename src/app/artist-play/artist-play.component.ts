import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '../api.service';
import { CommonModule } from '@angular/common';
import { AnnotationInterface, ArtistInterface, ReferentInterface, SearchResultInterface } from '../models/api-response.model';
import { debounceTime, distinctUntilChanged, scan, take, tap } from 'rxjs';
import { LoadingCircleComponent } from '../loading-circle/loading-circle.component';
import { FormControl, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SearchResultsComponent } from '../home/search-results/search-results.component';
import { TranslateModule } from '@ngx-translate/core';
import { FinalScoreComponent } from "../final-score/final-score.component";
import { GameService } from '../game.service';

@Component({
    selector: 'app-artist-play',
    standalone: true,
    templateUrl: './artist-play.component.html',
    styleUrl: './artist-play.component.less',
    imports: [
        CommonModule,
        LoadingCircleComponent,
        SearchResultsComponent,
        ReactiveFormsModule,
        FormsModule,
        TranslateModule,
        LoadingCircleComponent,
        FinalScoreComponent
    ]
})
export class ArtistPlayComponent {
  artistName: string | undefined;

  loadingPage: boolean = true;
  loadingGame: boolean = true;
  endGame: boolean = false;
  showNextModal: boolean = false;

  index: number = 0;
  score: number = 5000;

  songs: SearchResultInterface[] = [];
  currentSong: SearchResultInterface | undefined;

  currentSongReferents: ReferentInterface[] = [];
  shownReferents: ReferentInterface[] = [];

  previousGuesses: SearchResultInterface[] = [];

  guessSuggestionsShown: boolean = false;
  guessSuggestions: SearchResultInterface[] = [];
  loadingGuessSuggestions: boolean = false;
  
  guess: FormControl = new FormControl('');

  loadingLyrics: boolean = true;

  constructor(private route: ActivatedRoute, private api: ApiService, private game: GameService) { }

  showGuessSuggestions() {
    this.guessSuggestionsShown = true;
  }

  hideGuessSuggestions() {
    this.guessSuggestionsShown = false;
  }

  checkGuess(guessedSong: SearchResultInterface) {
    if (this.previousGuesses.find(song => song.id === guessedSong.id)) {
      return;
    }
    
    this.previousGuesses.push(guessedSong);
    if (guessedSong.id === this.currentSong?.id) {
      this.showNextModal = true;
    } else {
      this.score -= 100;
    }
  }

  onClickGuess(guessedSong: SearchResultInterface) {
    this.checkGuess(guessedSong);
    this.guessSuggestionsShown = false;
  }

  onSubmitGuess() {
    this.guessSuggestionsShown = false;
    this.api.search(this.guess.value).subscribe((results: SearchResultInterface[]) => {
      const guessedSong = results[0];
      this.checkGuess(guessedSong);
    });
  }

  nextRound() {
    this.index++;
    if (this.index === this.songs.length || this.index === 5) {
      this.game.saveGameResult(this.artistName!, this.score);
      this.endGame = true;
    } else {
      this.setUpGame();
    }
  }

  skipRound() {
    this.score -= 500;
    this.previousGuesses.push(this.currentSong!);
    this.showNextModal = true;
  }

  moreReferents() {
    if (this.currentSongReferents.length === 0) {
      return;
    }
    this.loadingLyrics = true;
    this.score -= 200;
    this.shownReferents.push(this.currentSongReferents[Math.floor(Math.random() * this.currentSongReferents.length)]);
    this.currentSongReferents = this.currentSongReferents.filter(referent => referent.id !== this.shownReferents[this.shownReferents.length - 1].id);
    this.loadingLyrics = false;
  }

  searchGuess(query: string) {
    this.guessSuggestions = [];
    this.showGuessSuggestions();
    this.loadingGuessSuggestions = true;
    this.api.search(query + " " + this.artistName).subscribe((results: SearchResultInterface[]) => {
      this.guessSuggestions = results.filter(result => result.primary_artist.id === this.currentSong?.primary_artist.id);
      this.loadingGuessSuggestions = false;
    });
  }

  setUpGame() {
    this.showNextModal = false;
    this.shownReferents = [];
    this.currentSongReferents = [];
    this.previousGuesses = [];
    this.currentSong = this.songs[Math.floor(Math.random() * this.songs.length)];
    this.songs = this.songs.filter(song => song.id !== this.currentSong?.id);
    this.loadingLyrics = true;
    this.api.getReferents(this.currentSong.id.toString())
    .subscribe((referents: ReferentInterface[]) => {
      console.log(referents);
      this.currentSongReferents = referents;
      this.shownReferents.push(referents[Math.floor(Math.random() * referents.length)]);
      this.currentSongReferents = this.currentSongReferents.filter(referent => referent.id !== this.shownReferents[0].id);
      this.loadingLyrics = false;
      this.loadingGame = false;
    });
  }
  ngOnInit() {
    const artistId = this.route.snapshot.paramMap.get('id')!;
    this.api.getArtist(artistId).subscribe((artist: ArtistInterface) => {
      this.artistName = artist.name;
      this.loadingPage = false;
    });
    this.api.getArtistSongs(artistId)
    .subscribe((artistSongs: SearchResultInterface[]) => {
      this.songs = artistSongs.filter(song => song.annotation_count > 5 && song.primary_artist.id.toString() === artistId);
      this.setUpGame();
    });

    this.guess.valueChanges.pipe(
      debounceTime(400),
      distinctUntilChanged()
    ).subscribe(query => {
      this.searchGuess(query);
    });
  }
}
