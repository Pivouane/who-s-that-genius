import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ArtistPlayComponent } from './artist-play.component';

describe('ArtistPlayComponent', () => {
  let component: ArtistPlayComponent;
  let fixture: ComponentFixture<ArtistPlayComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ArtistPlayComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ArtistPlayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
