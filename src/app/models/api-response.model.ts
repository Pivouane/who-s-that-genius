
interface BoundingBoxInterface {
  width: number;
  height: number;
}

interface AvatarInterface {
  url: string;
  boundingBox: BoundingBoxInterface;
}

export interface UserInterface {
  about_me: { dom: { tag: string } };
  api_path: string;
  avatar: {
    tiny: AvatarInterface;
    thumb: AvatarInterface;
    small: AvatarInterface;
    medium: AvatarInterface;
  };
  custom_header_image_url: string | null;
  email: string;
  followed_users_count: number;
  followers_count: number;
  header_image_url: string;
  human_readable_role_for_display: string | null;
  id: number;
  iq: number;
  iq_for_display: string;
  login: string;
  name: string;
  photo_url: string;
  role_for_display: string | null;
  roles_for_display: string[];
  unread_groups_inbox_count: number;
  unread_main_activity_inbox_count: number;
  unread_newsfeed_inbox_count: number;
  url: string;
  current_user_metadata: {
    permissions: string[];
    excluded_permissions: string[];
    interactions: { following: boolean };
    features: string[];
  };
  unread_messages_count: number;
  artist: ArtistInterface | null;
  stats: {
    annotations_count: number;
    answers_count: number;
    comments_count: number;
    forum_posts_count: number;
    pyongs_count: number;
    questions_count: number;
    transcriptions_count: number;
  };
}

export interface ApiResponseInterface {
  meta: { status: number };
  response: {
    user: UserInterface;
    hits: HitInterface[];
    song: SearchResultInterface;
    songs: SearchResultInterface[];
    referents: ReferentInterface[];
    artist: ArtistInterface;
  };
}


export interface SearchResultInterface {
    annotation_count: number;
    api_path: string;
    artist_names: string;
    full_title: string;
    header_image_thumbnail_url: string;
    header_image_url: string;
    id: number;
    lyrics_owner_id: number;
    lyrics_state: string;
    path: string;
    pyongs_count: number;
    relationships_index_url: string;
    release_date_for_display: string;
    release_date_with_abbreviated_month_for_display: string;
    song_art_image_thumbnail_url: string;
    song_art_image_url: string;
    title: string;
    title_with_featured: string;
    url: string;
    featured_artists: ArtistInterface[];
    primary_artist: ArtistInterface;
}

export interface ArtistInterface {
  api_path: string;
  header_image_url: string;
  id: number;
  image_url: string;
  is_meme_verified: boolean;
  is_verified: boolean;
  name: string;
  url: string;
}

export interface HitInterface {
  index: string;
  type: string;
  result: SearchResultInterface;
}

export interface ReferentInterface {
  _type: string;
  annotator_id: number;
  annotator_login: string;
  api_path: string;
  classification: string;
  fragment: string;
  id: number;
  is_description: boolean;
  path: string;
  range: Range;
  song_id: number;
  url: string;
  verified_annotator_ids: number[];
  annotations: AnnotationInterface[];
}

export interface AnnotationInterface {
  api_path: string;
  body: BodyInterface;
  comment_count: number;
  community: boolean;
  has_voters: boolean;
  id: number;
  pinned: boolean;
  share_url: string;
  state: string;
  url: string;
  verified: boolean;
  votes_total: number;
  authors: AuthorInterface[];
}

export interface BodyInterface {
  plain: string;
}

export interface AuthorInterface {
  attribution: number;
  pinned_role: string;
  user: UserInterface;
}
